ifndef BIN
	BIN = ../murbin
endif

CXX = g++
CXXFLAGS = -std=c++11 -Wall

INCLUDES = -I. 
LIBS = -lzmq -lmraa -lconfig++

CONFIGS = config/
CALIBRATION = transform.txt

ccps = $(wildcard src/*.cpp)
objs = $(patsubst %.cpp,%.o,$(ccps))
ddds = $(wildcard *.d)

all: murserver

include $(ddds)

murserver: $(objs)
	mkdir -p $(BIN)
	$(CXX) $(CXXFLAGS) $^ $(INCLUDES) $(LIBS) -o $(BIN)/murserver
	cp -r $(CONFIGS) $(BIN)
	cp $(CALIBRATION) $(BIN)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -MD $< $(INCLUDES) -o $@

clean:
	rm -rf src/*.d
	rm -rf src/*.o
	rm $(BIN)/murserver
	rm -r $(BIN)/$(CONFIGS)
	rm $(BIN)/$(CALIBRATION)
