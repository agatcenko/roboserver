#include "uart_reply_storage.h"

using namespace std;

UartReplyStorage::UartReplyStorage()
{

}

AdcInfo UartReplyStorage::getAdcInfo() const
{
    lock_guard<mutex> lock(m_dataMutex);
    return m_adcInfo;
}

void UartReplyStorage::setAdcInfo(AdcInfo info)
{
    lock_guard<mutex> lock(m_dataMutex);
    m_adcInfo = info;
}

array<uint8_t, DEVICES_NUM> UartReplyStorage::getDevicesStatus() const
{
    lock_guard<mutex> lock(m_dataMutex);
    return m_devicesStatus;
}

void UartReplyStorage::setDevicesStatus(array<uint8_t, DEVICES_NUM> status)
{
    lock_guard<mutex> lock(m_dataMutex);
    m_devicesStatus = status;
}