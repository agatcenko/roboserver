#pragma once

#include "lsm_controller.h"

#include <array>
#include <memory>
#include <string>
#include <fstream>

const int AXES_NUM = 3;

using MagValues = std::array<int16_t, AXES_NUM>; 

class MagDataCollector
{
public:
    MagDataCollector();
    ~MagDataCollector();

    void collect();

private:
    MagValues m_magMin;
    MagValues m_magMax;

    const std::string m_rawMagFileName = "raw_mag.txt";
    const std::string m_extremumMagFileName = "extremum_mag.txt";

    std::ofstream m_rawMagFile;
    std::ofstream m_extremumMagFile;

    const int m_lsmBus = 1;
    std::unique_ptr<LsmController> m_lsmController;

    const int m_captureTimeout = 100; // ms
    const int m_captureDuration = 30000; // ms, 30 s
};