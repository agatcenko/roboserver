#pragma once

// gyro data
#define OUT_X_L_G   0x28
#define OUT_X_H_G   0x29
#define OUT_Y_L_G   0x2A
#define OUT_Y_H_G   0x2B
#define OUT_Z_L_G   0x2C
#define OUT_Z_H_G   0x2D

// mag data
#define OUT_X_L_M   0x08
#define OUT_X_H_M   0x09
#define OUT_Y_L_M   0x0A
#define OUT_Y_H_M   0x0B
#define OUT_Z_L_M   0x0C
#define OUT_Z_H_M   0x0D

// accel data
#define OUT_X_L_A   0x28
#define OUT_X_H_A   0x29
#define OUT_Y_L_A   0x2A
#define OUT_Y_H_A   0x2B
#define OUT_Z_L_A   0x2C
#define OUT_Z_H_A   0x2D

// control gyro
#define CTRL_REG1_G     0x20
#define CTRL_REG2_G     0x21
#define CTRL_REG3_G     0x22
#define CTRL_REG4_G     0x23
#define CTRL_REG5_G     0x24

// control accel and mag
#define CTRL_REG0_XM    0x1F
#define CTRL_REG1_XM    0x20
#define CTRL_REG2_XM    0x21
#define CTRL_REG3_XM    0x22
#define CTRL_REG4_XM    0x23
#define CTRL_REG5_XM    0x24
#define CTRL_REG6_XM    0x25
#define CTRL_REG7_XM    0x26

#define FIFO_CTRL_REG   0x2E
#define FIFO_SRC_REG    0x2F

#define FIFO_CTRL_REG_G 0x2E
#define FIFO_SRC_REG_G  0x2F

// who am i register
#define WHO_AM_I_G      0x0F
#define WHO_AM_I_XM     0x0F

#define OUT_TEMP_L_XM   0x05
#define OUT_TEMP_H_XM   0x06