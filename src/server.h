#pragma once

#include "zmq.hpp"
#include "timer.h"
#include "msg_types.h"
#include "time_utils.h"
#include "uart_utils.h"
#include "config_utils.h"
#include "compass_utils.h"
#include "lsm_controller.h"
#include "uart_reply_storage.h"
#include "uart_request_storage.h"

#include <mutex>
#include <atomic>
#include <memory>
#include <string>
#include <thread>

/*!
    \brief Class for convertion values using linear equation.

    The most common example of use is conversion values from ADC 
    (Analog-to-digital converter) to real phisical quantities such as meters.  
*/
struct LinearConverter
{
    uint16_t x1 = 0; //!< X coordinate of first point
    uint16_t x2 = 1; //!< X coordinate of second point
    float y1 = 0.0f; //!< Y coordinate of first point
    float y2 = 1.0f; //!< Y coordinates of second point
    
    //! Set coordinates \f$(x,y)\f$ for two points, that define the line
    /*! 
        \param _x1 X coordinate for the lowest value
        \param _y1 Y coordinate for the lowest value
        \param _x2 X coordinate for the highest value
        \param _y2 Y coordinate for the highest value
    */
    void setValues(uint16_t _x1, uint16_t _x2, float _y1, float _y2)
    {
        x1 = _x1; x2 = _x2;
        y1 = _y1; y2 = _y2;
    }
    //! Convert value
    /*!
        Using formula \f$y=\frac{(x-x_1)(y_2-y_1)}{(x_2-x_1)}\f$, where \f$(x_1 \ne x_2)\f$
        \param x The initial value to be converted
        \return The converted value
    */
    float convert(uint16_t x)
    {
        return y1 + ((float)x - x1) * (y2 - y1) / (x2 - x1);
    }
};

/*!
    \brief Main singleton class
    
    Communicate with other processes with zmq messaging.
    Communicate with vehicle devices through uart and i2c.
*/
class Server
{
public:
    static Server& instance();
    ~Server();

    void start(); //!< Initialize timers
    void update(); //!< Listen zmq messages
private:
    Server();
    Server(Server const &) = delete;
    Server& operator=(Server const &) = delete;

    void updateAdcData(); //!< Send request for new adc info. Update m_adcInfo
    void updateDevicesStatus(); //!< Send request for new devices status
    
    void handleExecServerCmd(zmq::message_t &message); //!< Handle zmq msg with command for execserver
    void updateThrustersPower(zmq::message_t &message); //!< Handle zmq msg with new thrusters power 

    void publishVehicleInfo(); //!< Send with zmq VehicleInfo struct to murapi
    void publishButtonPressed(); //!< Send with zmq button event to execserver
    void updateAndPublishStatus(); //!< Send with zmq StatusInfo struct to muride

    //! Get number of available cameras 
    /*!
        Check availability of device /dev/videoX, X = [0, 1]
    */
    uint8_t getCamerasStatus();

    // Thread handlers

    //! Thread handler for uart communication
    /*!
        \param [in] port string representation of serial device
    */
    void handleUart(std::string port);

    //! Thread handler for compass processing
    /*!
        \param [in] bus i2c bus number in system
        \param [in] readTimeout period in milliseconds for read raw compass data 
    */
    void handleCompass(int bus, unsigned int readTimeout);

    // config values (see server.cfg)

    REG_CONFIG_VALUE(int, m_version) //!< Version number of the server software
    REG_CONFIG_VALUE(int, m_lsmBus) //!< The i2c interface bus number for compass communication
    REG_CONFIG_VALUE(std::string, m_uartPort) //!< String path of the serial interface for communication with pheripheral
    
    REG_CONFIG_VALUE(unsigned int, m_statusTimeout) //!< Timeout in milliseconds for status timer \sa m_statusTimer
    REG_CONFIG_VALUE(unsigned int, m_publishTimeout) //!< Timeout in milliseconds for publish timer \sa m_publishTimer
    REG_CONFIG_VALUE(unsigned int, m_compassTimeout) //!< Timeout in milliseconds for compass timer \sa m_compassTimer

    REG_CONFIG_VALUE(int, m_depthX1) //!< First fixed point adc depth value
    REG_CONFIG_VALUE(int, m_depthX2) //!< Second fixed point adc depth value
    REG_CONFIG_VALUE(float, m_depthY1) //!< First fixed point real depth in sm
    REG_CONFIG_VALUE(float, m_depthY2) //!< Second fixed point real depth in sm

    REG_CONFIG_VALUE(std::string, m_clientReplyPipe) //!< local pipe for control messages: exec | api -> server
    REG_CONFIG_VALUE(std::string, m_statusPublishPipe) //!< tcp port for status data: server -> ide
    REG_CONFIG_VALUE(std::string, m_clientPublishPipe) //!< local pipe for vehicle data: server -> api
    REG_CONFIG_VALUE(std::string, m_clientRequestPipe) //!< local pipe for button status: server -> exec

    std::thread m_uartThread; //!< Thread object for uart communication
    std::thread m_compassThread; //!< Thread object for compass processing

    std::mutex m_compassDataMutex; //!< Mutex object for compass data access sync
    std::atomic<bool> m_isRunning { true }; //!< Flag is set when process is active

    // zmq objects
    std::unique_ptr<zmq::context_t> m_context; //!< zmq context
    std::unique_ptr<zmq::socket_t> m_clientReply; //!< zmq socket, exec | api -> server
    std::unique_ptr<zmq::socket_t> m_clientRequest; //!< zmq socket, server -> exec
    std::unique_ptr<zmq::socket_t> m_clientPublish; //!< zmq socket, server -> api
    std::unique_ptr<zmq::socket_t> m_statusPublish; //!< zmq socket, server -> ide

    // Timers
    std::shared_ptr<Timer> m_statusTimer; //!< Timer object for sending StatusInfo data
    std::shared_ptr<Timer> m_publishTimer; //!< Timer object for sending VehicleInfo data

    AdcInfo m_adcInfo; //!< Store last adc data
    CompassInfo m_compassInfo; //!< Store last compass data
    LinearConverter m_lcDepth; //!< LinearConverter instance for converting raw depth data to centimeters

    UartReplyStorage m_uartReplyStorage; //!< Stores reply uart data 
    UartRequestStorage m_uartRequestStorage; //!< Stores and executes uart requests 
};