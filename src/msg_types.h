#pragma once

#include <cstdint>

//! Maximum number of thrusters
static const int8_t thrustersNum = 4;

//! Number of available devices (Thrusters, Sensors)
static const std::size_t DEVICES_NUM = 6;

/*!
    \brief Structure for store status data about vehicle

    This structure used in ide.  
*/
struct StatusInfo
{
    uint8_t devicesTypes[DEVICES_NUM]; //!< Array with devices types
    float yaw = 0.0f; //!< Yaw [-180.0; 180.0]
    float pitch = 0.0f; //!< Pitch [-180.0; 180.0]
    float roll = 0.0f; //!< Roll [-180.0; 180.0]
    float depth = 0.0f; //!< Depth in centimeters
    uint8_t leak = 0; //!< Leak flag, 1 - Bad, 0 - Good
    uint8_t version = 0; //!< Version of server
    uint8_t cameras = 0; //!< Number of cameras conneted to vehicle
};

/*!
    \brief Structure for store raw adc data from supervisor
*/
struct AdcInfo
{
    uint8_t leakStatus = 0; //!< Leak flag, 1 - Bad, 0 - Good
    uint8_t buttonStatus = 0; //!< Button flag
    uint16_t systemTemp = 0; //!< System temperature
    uint16_t batteryTemp = 0; //!< Battery temperature
    uint16_t batteryVolts = 0; //!< Battery volts
    uint16_t depth = 0; //!< Depth
};

/*!
    \brief Structure for store compass info
*/
struct CompassInfo
{
    float dus = 0.0F; // angular velocity sensor data
    float yaw = 0.0F; //!< Yaw [-180.0; 180.0]
    float roll = 0.0F; //!< Roll [-180.0; 180.0]
    float pitch = 0.0F; //!< Pitch [-180.0; 180.0]
};

/*!
    \brief Structure for store positional info about vehicle

    This structure used in api.
*/
struct VehicleInfo
{
    float roll = 0.0F; //!< Roll [-180.0; 180.0]
    float pitch = 0.0F; //!< Pitch [-180.0; 180.0]
    float depth = 0.0F; //!< Depth in centimeters
    float dus = 0.0F; // angular velocity sensor data
    float yaw = 0.0F; //!< Yaw [-180.0; 180.0]
};

/*!
    \brief Structure for store power of thruster
*/
struct Thruster
{
    int8_t power; //!< Power [-100; 100]
    uint8_t id; //!< Thruster id
    //! Struct constructor
    Thruster(int8_t _power = 0, uint8_t _id = 0) : power(_power), id(_id) {}
};

/*!
    \brief Structure for store powers of all thrusters
*/
struct ThrustersPower
{
    Thruster thrusters[thrustersNum]; //!< Array of Thruster structures
};

#define THRUSTERS_MSG_SIZE sizeof(ThrustersPower)
#define EXEC_SERVER_MSG_SIZE sizeof(uint8_t)