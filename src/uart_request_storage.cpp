#include "uart_request_storage.h"

using namespace std;

UartRequestStorage::UartRequestStorage()
{

}

void UartRequestStorage::devicesStatusRequest()
{
    lock_guard<mutex> lock(m_storageMutex);
    updateQueue(RequestType::DEVICES_STATUS);
}

void UartRequestStorage::altimeterInfoRequest()
{
    lock_guard<mutex> lock(m_storageMutex);
    updateQueue(RequestType::ALTIMETER_INFO);   
}

void UartRequestStorage::supervisorInfoRequest()
{
    lock_guard<mutex> lock(m_storageMutex);
    updateQueue(RequestType::SUPERVISOR_INFO);   
}

void UartRequestStorage::deviceTypeRequest(uint8_t id)
{
    lock_guard<mutex> lock(m_storageMutex);
    updateQueue(RequestType::DEVICE_TYPE);

    m_deviceId = id;
}

void UartRequestStorage::missionStatusRequest(uint8_t status)
{
    lock_guard<mutex> lock(m_storageMutex);
    updateQueue(RequestType::MISSION_STATUS);

    m_missionStatus = status;
}
void UartRequestStorage::thrustersPowerRequest(ThrustersPower power)
{
    lock_guard<mutex> lock(m_storageMutex);
    updateQueue(RequestType::THRUSTERS_POWER);

    m_thrustersPower = power;
}

bool UartRequestStorage::isNoRequests() const
{
    lock_guard<mutex> lock(m_storageMutex);
    return m_requests.empty();
}

RequestType UartRequestStorage::getRequest()
{
    lock_guard<mutex> lock(m_storageMutex);
    RequestType type = RequestType::UNKNOWN_REQUEST;
    if (!m_requests.empty()) {
        type = m_requests.front();
        m_requests.pop();
        m_waitingRequests.erase(type);    
    }
    
    return type; 
}

uint8_t UartRequestStorage::getDeviceId() const
{
    lock_guard<mutex> lock(m_storageMutex);
    return m_deviceId;
}

uint8_t UartRequestStorage::getMissionStatus() const
{
    lock_guard<mutex> lock(m_storageMutex);
    return m_missionStatus;
}

ThrustersPower UartRequestStorage::getThrustersPower() const
{
    lock_guard<mutex> lock(m_storageMutex);
    return m_thrustersPower;
}

void UartRequestStorage::updateQueue(RequestType type)
{
    // Add in queue only if queue doesn`t have request with such type  
    if (!m_waitingRequests.count(type)) {
        m_requests.push(type);
        m_waitingRequests.insert(type);
    }
}