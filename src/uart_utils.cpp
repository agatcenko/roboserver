#include "uart_utils.h"
#include "time_utils.h"

#include <chrono>
#include <thread>
#include <iostream>
#include <unistd.h>
#include <cstdint>
#include <iterator>
#include <algorithm>

using namespace std;
using namespace mraa;

const uint8_t H_BYTE = 8;

UartManager::UartManager(string device, UartReplyStorage &replyStorage, UartRequestStorage &requestStorage) :
    m_uartReplyStorage(replyStorage),
    m_uartRequestStorage(requestStorage)
{
    m_dev = new Uart(device);

    m_receiveBuffer.resize(UINT8_MAX, 0);
    m_devicesAddr.fill(UNDEF_ADDR);
    m_devicesAddr = { 10, 20, 30, 40 }; // addresses for thrusters
    m_devicesStatus.fill(UNDEF_DEVICE);

    if (setUartSettings() != 0) {
        LOG << "Error while setUartSettings" << endl;
    }

    m_dev->flush();
    
    registerIncomeMsg(ERROR_MSG, &UartManager::errorHandler);
    registerIncomeMsg(DEVICE_TYPE, &UartManager::deviceTypeHandler);
    registerIncomeMsg(THRUSTER_STATUS, &UartManager::thrusterStatusHandler);
    registerIncomeMsg(SUPERVISOR_DATA, &UartManager::supervisorDataHandler);
    registerIncomeMsg(ALTIMETER_DATA, &UartManager::altimeterDataHandler);

    registerRequestHandler(RequestType::DEVICE_TYPE, &UartManager::getDeviceType);
    registerRequestHandler(RequestType::DEVICES_STATUS, &UartManager::updateDevicesStatus);
    registerRequestHandler(RequestType::ALTIMETER_INFO, &UartManager::getAltimeterInfo);
    registerRequestHandler(RequestType::SUPERVISOR_INFO, &UartManager::getSupervisorInfo);
    registerRequestHandler(RequestType::MISSION_STATUS, &UartManager::sendMissionStatus);
    registerRequestHandler(RequestType::THRUSTERS_POWER, &UartManager::sendThrustersPower);

    m_silenceTimer = make_shared<OneShotTimer>(m_silenceTimeout);
}

UartManager::~UartManager()
{
    delete m_dev;
}

void UartManager::getDeviceType()
{
    uint8_t id = m_uartRequestStorage.getDeviceId();
    LOG << "getDeviceType with id: " << (int)id << endl;
    m_devicesStatus[currentDeviceIt] = UNDEF_DEVICE;
    vector<uint8_t> data (1, id);
    if (sendPackage(REQUEST_DEVICE_TYPE, id,
                    PackageConfirmation::WITH_CONFIRM, data)) {
        LOG << "Error while send package in getDeviceType" << endl;
        return;
    }

    getResponse();

    currentDeviceIt = (currentDeviceIt + 1) % DEVICES_NUM;
}

void UartManager::getSupervisorInfo()
{
    LOG << "getSupervisorInfo" << endl;
    vector<uint8_t> data (1, m_isButtonProcessed);
    if (sendPackage(REQUEST_SUPERVISOR_DATA, SUPERVISOR_ID, 
                    PackageConfirmation::WITH_CONFIRM, data)) {
        LOG << "Error while send package in getSupervisorInfo" << endl;
        return;
    }

    getResponse();
}

void UartManager::getAltimeterInfo()
{
    LOG << "getAltimeterInfo" << endl;
    vector<uint8_t> data = vector<uint8_t>();
    if (sendPackage(REQUEST_ALTIMETER_DATA, ALTIMETER_ID,
                    PackageConfirmation::WITH_CONFIRM, data)) {
        LOG << "Error while send package in getAltimeterInfo" << endl;
        return;
    }

    getResponse();
}

void UartManager::getThrusterStatus(uint8_t id)
{
    LOG << "getThrusterStatus" << endl;
    vector<uint8_t> data = vector<uint8_t>();
    if (sendPackage(REQUEST_THRUSTER_STATUS, id, 
                    PackageConfirmation::WITH_CONFIRM, data)) {
        LOG << "Error while send package in getThrusterStatus" << endl;
        return;
    }

    getResponse();
}

void UartManager::sendMissionStatus()
{
    uint8_t status = m_uartRequestStorage.getMissionStatus();
    vector<uint8_t> data (1, status);
    if (sendPackage(CONTROL_LED_MODE, SUPERVISOR_ID,
                    PackageConfirmation::WITHOUT_CONFIRM, data)) {
        LOG << "Error while send package in sendMissionStatus" << endl;
        return;
    }
}    

void UartManager::sendThrustersPower()
{
    ThrustersPower power = m_uartRequestStorage.getThrustersPower();
    vector<uint8_t> data;
    data.push_back(static_cast<uint8_t>(thrustersNum));
    for (const auto t : power.thrusters) {
        data.push_back(t.id);
        data.push_back(t.power);
        LOG << "Send power: " << (int)t.power << " for thruster: " << (int)t.id << endl;
    }

    if (sendPackage(CONTROL_THRUSTERS_BROADCAST, BROADCAST,
                PackageConfirmation::WITHOUT_CONFIRM, data)) {
        LOG << "Error while send package in sendThrustersPower" << endl;
        return;
    }
}

void UartManager::updateDevicesStatus()
{
    int undefAddrCounter = 0;
    while (m_devicesAddr[currentDeviceIt] == UNDEF_ADDR) {
        if (undefAddrCounter == DEVICES_NUM) {
            LOG << "No available devices for update status" << endl;
            return;
        }
        undefAddrCounter++;
        currentDeviceIt = (currentDeviceIt + 1) % DEVICES_NUM;    
    }

    m_uartRequestStorage.deviceTypeRequest(m_devicesAddr[currentDeviceIt]);
}

bool UartManager::isReady() const
{
    return m_silenceTimer->isCompleted();
}

void UartManager::execRequest()
{
    RequestType type = m_uartRequestStorage.getRequest();
    if (m_requestHandlers.count(type)) {
        auto handler = m_requestHandlers[type];
        (this->*handler)();  
    }
    
    m_silenceTimer->reset();
}

bool UartManager::isNoRequests() const
{
    return m_uartRequestStorage.isNoRequests();
}

int UartManager::setUartSettings()
{
    if (m_dev->setBaudRate(BAUDRATE) != SUCCESS) {
        LOG << "Error setting baudrate on UART" << endl;
        return -1;
    }

    if (m_dev->setMode(BYTE_SIZE, UART_PARITY_NONE, STOP_BITS) != SUCCESS) {
        LOG << "Error setting parity on UART" << endl;
        return -1;
    }

    if (m_dev->setFlowcontrol(XON_XOFF, RTS_CTS) != SUCCESS) {
        LOG << "Error setting flow control UART" << endl;
        return -1;
    }

    return 0;
}

void UartManager::getResponse()
{
    vector<uint8_t> package;
    if (m_dev->dataAvailable(RESPONSE_WAIT)) {
        int inputSize = m_dev->read((char*)m_receiveBuffer.data(), m_receiveBuffer.size());
        if (inputSize) {
            copy(m_receiveBuffer.begin(), m_receiveBuffer.begin() + inputSize, back_inserter(package));
        }
    }
    
    if (package.size()) {
        processInputPackage(package);
    }
    else {
        LOG << "No available data" << endl;
    }
}

int UartManager::sendPackage(uint8_t msgId, uint8_t receiverId,
                             PackageConfirmation confirmFlag, vector<uint8_t> &data)
{
    if (m_dev->dataAvailable()) {
        int trashSize = m_dev->read((char*)m_receiveBuffer.data(), m_receiveBuffer.size());
        LOG << "read trash from channel before write" << endl;
        for (int i = 0; i < trashSize; ++i) {
            cout << (int)m_receiveBuffer[i] << " ";
        }
        cout << endl;
    }

    vector<uint8_t> package (SERVICE_INFO_SIZE + data.size());
    size_t packageSize = package.size();

    package[START_PACKAGE_NUM_1] = START_PACKAGE_VAL_1;
    package[START_PACKAGE_NUM_2] = START_PACKAGE_VAL_2;

    package[PACKAGE_SIZE_NUM] = (uint8_t)packageSize;
    package[TRANSMITTER_BYTE_NUM] = EDISON_ID;
    package[RECEIVER_BYTE_NUM] = receiverId;
    package[CONFIRM_FLAG_NUM] = static_cast<uint8_t>(confirmFlag);
    package[MSG_ID_NUM] = msgId;

    copy(data.begin(), data.end(), package.begin() + DATA_OFFSET);

    uint16_t crc = calculateCRC(package.data(), packageSize - 3);
    package[packageSize - 3] = crc >> H_BYTE;
    package[packageSize - 2] = crc & 0xFF;
    package[packageSize - 1] = END_PACKAGE_VAL;

    if (m_dev->write((const char*)package.data(), packageSize) == -1) {
        return -1;
    }

    return 0;
}

void UartManager::processInputPackage(vector<uint8_t> &package)
{   
    try {
        bool findFirstByte = false;
        for (size_t i = 0; i < package.size() - 1; ++i) {
            if (package.at(i) == START_PACKAGE_VAL_1) {
                findFirstByte = true;
                continue;
            }
            if (findFirstByte) {
                if (package.at(i) == START_PACKAGE_VAL_2) {
                    int realSize = (int)package.at(i + 1);
                    if (package.at(i + realSize - 2) == END_PACKAGE_VAL) {
                        auto begin = package.begin() + i - 1;
                        vector<uint8_t> p (begin, begin + realSize);
                        package = p;
                        break; 
                    }
                }
                findFirstByte = false;
            }
        }

        size_t packageSize = package.size();
        if (package.at(START_PACKAGE_NUM_1) != START_PACKAGE_VAL_1 ||
            package.at(START_PACKAGE_NUM_2) != START_PACKAGE_VAL_2 ||
            package.at(packageSize - 1) != END_PACKAGE_VAL)
        {
            LOG << "First and last bytes of the package are incorrect" << endl;
            for (const auto b : package) {
                cout << (int)b << " ";
            }
            cout << endl;
            return;
        }

        uint16_t crc = calculateCRC(package.data(), packageSize - 3);
        if ((package.at(packageSize - 3) != (crc >> 8)) ||
            (package.at(packageSize - 2) != (crc & 0xFF)))
        {
            LOG << "Crc of the package is incorrect" << endl;
            return;
        }

        if (package.at(RECEIVER_BYTE_NUM) == EDISON_ID) {
            uint8_t msgId = package.at(MSG_ID_NUM);
            if (m_msgHandlers.count(msgId)) {
                if (packageSize >= SERVICE_INFO_SIZE) {
                    uint8_t dataSize = packageSize - SERVICE_INFO_SIZE;
                    vector<uint8_t> data (package.data() + DATA_OFFSET, 
                                          package.data() + DATA_OFFSET + dataSize);
                    auto handler = m_msgHandlers[msgId];
                    (this->*handler)(data);
                } 
                else {
                    LOG << "Income package is too small" << endl;
                    return;
                }
            }
            else {
                LOG << "Income msg with id: " << (int)msgId << 
                " has no registered handler" << endl;
                return;
            }
        }
    }
    catch (const out_of_range &e) {
        LOG << "Out of range in process input package: " << e.what() << endl;
        return;
    }
}

uint16_t UartManager::updateCRC(uint16_t acc, const uint8_t input)
{
    // Create the CRC "dividend" for polynomial arithmetic (binary arithmetic
    // with no carries)
    acc ^= (input << 8);

    // "Divide" the poly into the dividend using CRC XOR subtraction
    // CRC_acc holds the "remainder" of each divide
    // Only complete this division for 8 bits since input is 1 byte

    for (uint8_t i = 0; i < 8; i++) {
        // Check if the MSB is set (if MSB is 1, then the POLY can "divide"
        // into the "dividend")

        if ((acc & 0x8000) == 0x8000) {
            acc <<= 1;
            acc ^= POLY;
        }
        else {
            acc <<= 1;
        }
    }

    return acc;
}

uint16_t UartManager::calculateCRC(const uint8_t *data, const int32_t len)
{
    uint16_t crcout = SEED;

    for (int32_t i = 0; i < len; i++) {
        crcout = updateCRC(crcout, data[i]);
    }

    return crcout;
}

void UartManager::registerRequestHandler(RequestType type, void (UartManager::*handler)())
{
    m_requestHandlers[type] = handler;
}

void UartManager::registerIncomeMsg(uint8_t msgId, void (UartManager::*handler)(const std::vector<uint8_t>&))
{
    m_msgHandlers[msgId] = handler;
}

void UartManager::errorHandler(const vector<uint8_t> &data)
{
    LOG << "Received msg about error" << endl;
}

void UartManager::deviceTypeHandler(const std::vector<uint8_t> &data)
{
    LOG << "Received msg with device type" << endl;
    LOG << "type: " << (int)data.at(DEVICE_TYPE_OFFSET) << endl;
    m_devicesStatus[currentDeviceIt] = data.at(DEVICE_TYPE_OFFSET);

    m_uartReplyStorage.setDevicesStatus(m_devicesStatus);
}

void UartManager::thrusterStatusHandler(const vector<uint8_t> &data)
{
    LOG << "Received msg with thruster status" << endl;
    LOG << "current: " <<  (data.at(CURRENT_OFFSET_H) << H_BYTE) + data.at(CURRENT_OFFSET_L) << endl;
    LOG << "rpm: " <<  (data.at(RPM_OFFSET_H) << H_BYTE) + data.at(RPM_OFFSET_L) << endl;
    LOG << "power: " << (int)data.at(POWER_OFFSET) << endl;
}

void UartManager::supervisorDataHandler(const vector<uint8_t> &data)
{
    LOG << "Received msg with supervisor data" << endl;
    m_supervisorInfo.batteryVolts = (data.at(BATTERY_OFFSET_H) << H_BYTE) + data.at(BATTERY_OFFSET_L);
    m_supervisorInfo.depth = (data.at(DEPTH_OFFSET_H) << H_BYTE) + data.at(DEPTH_OFFSET_L);
    m_supervisorInfo.systemTemp = (data.at(TEMP_OFFSET_H) << H_BYTE) + data.at(TEMP_OFFSET_L);
    uint8_t buttonAndLeak = data.at(BUTTON_LEAK_OFFSET);
    switch (buttonAndLeak) {
        case 0x00:
            m_supervisorInfo.leakStatus = 0;
            m_supervisorInfo.buttonStatus = 0;
            break;

        case 0x0A:
            m_supervisorInfo.leakStatus = 0;
            m_supervisorInfo.buttonStatus = 1;
            break;

        case 0xA0:
            m_supervisorInfo.leakStatus = 1;
            m_supervisorInfo.buttonStatus = 0;
            break;

        case 0xAA:
            m_supervisorInfo.leakStatus = 1;
            m_supervisorInfo.buttonStatus = 1;
            break;

        default:
            break;
    }

    m_isButtonProcessed = m_supervisorInfo.buttonStatus;

    m_uartReplyStorage.setAdcInfo(m_supervisorInfo);
    m_supervisorInfo.buttonStatus = 0;
}

void UartManager::altimeterDataHandler(const vector<uint8_t> &data)
{
    uint16_t altitude;
    altitude = (data.at(ALTITUDE_OFFSET_H) << H_BYTE) + data.at(ALTITUDE_OFFSET_L);
    LOG << "Altitude: " << altitude << endl;
}