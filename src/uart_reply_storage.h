#pragma once

#include "msg_types.h"

#include <array>
#include <mutex>

/*!
    \brief Class that handle data from input uart messages
*/
class UartReplyStorage
{
public:
    UartReplyStorage();

    AdcInfo getAdcInfo() const; //!< Getter for AdcInfo struct
    void setAdcInfo(AdcInfo info); //!< Setter for AdcInfo struct

    std::array<uint8_t, DEVICES_NUM> getDevicesStatus() const; //!< Getter for devices status
    void setDevicesStatus(std::array<uint8_t, DEVICES_NUM> status); //!< Setter for devices status

private:
    mutable std::mutex m_dataMutex; //!< Mutex object for data access

    AdcInfo m_adcInfo;
    std::array<uint8_t, DEVICES_NUM> m_devicesStatus;
};