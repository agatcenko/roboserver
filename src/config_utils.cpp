#include "config_utils.h"

using namespace std;
using namespace libconfig;

ConfigReader& ConfigReader::instance()
{
    static ConfigReader config;
    return config;
}

ConfigReader::ConfigReader()
{
    try {
        m_cfg.readFile(m_configFileName.c_str());
    }
    catch(const FileIOException& e) {
        cout << "I/O error while reading config file " << m_configFileName << endl;
    }
    catch(const ParseException& e) {
        cout << "Parse error at " << e.getFile() << ":" << e.getLine() << " - " << e.getError() << endl;
    }
}

ConfigReader::~ConfigReader()
{

}