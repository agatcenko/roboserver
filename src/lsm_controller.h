#pragma once

#include "lsm_types.h"
#include "lsm_registers.h"

#include <array>
#include <memory>
#include <string>
#include <mraa.hpp>

struct MagCalibrationData
{
    std::array<float, AxisDOF> bias;
    std::array<std::array<float, AxisDOF>, AxisDOF> transform;
};

class LsmController
{
public:
    LsmController(int bus);
    
    void setFrequency(mraa::I2cMode frequency);
    void setAddresses(uint8_t gAddress, uint8_t xmAddress);

    LsmSettings getSettings() const;

    int16_t readTemperature() const;
    std::array<int16_t, AxisDOF> readMag() const;
    std::array<int16_t, AxisDOF> readGyro() const;
    std::array<int16_t, AxisDOF> readAccel() const;

private:
    std::unique_ptr<mraa::I2c> m_i2c;

    uint8_t m_gAddress;
    uint8_t m_xmAddress;
    mraa::I2cMode m_frequency;

    LsmSettings m_lsmSettings;

    void initLsm() const;
    void calcBiases();
    void loadMagCalibrationData();

    uint8_t readReg(uint8_t i2cAddress, uint8_t regAddress) const;
    void readRegs(uint8_t i2cAddress, uint8_t regAddress, uint8_t* dest, uint8_t len) const;
    void writeReg(uint8_t i2cAddress, uint8_t regAddress, uint8_t data) const;

    uint8_t gReadReg(uint8_t regAddress) const;
    uint8_t xmReadReg(uint8_t regAddress) const;
    void gWriteReg(uint8_t regAddress, uint8_t data) const;
    void xmWriteReg(uint8_t regAddress, uint8_t data) const;

    void initMag() const;
    void initGyro() const;
    void initAccel() const;

    void setMagScale() const;
    void setGyroScale() const;
    void setAccelScale() const;

    // Set output data rate and bandwidth
    void setMagODR() const;
    void setGyroODR() const;
    void setAccelODR() const;

    // Set accel anti-alias filter bandwidth
    void setAccelABW() const;

    std::array<int16_t, AxisDOF> readSensor(uint8_t i2cAddress, uint8_t regAddress) const;

    bool m_calibrateMag = false;
    MagCalibrationData m_magCalibration;
    const std::string m_magCalibrationFileName = "/home/root/murbin/transform.txt";
};
