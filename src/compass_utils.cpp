#include "compass_utils.h"

#include <iostream>

using namespace std;

void Orientation::changeOrientation(int down_axis)
{
    if (down_axis == 0) {
        x = 2; y = 1; z = 0;
        magSign = { 1, -1, -1 };
        accelSign = gyroSign = { -1, -1, -1 };
    }
    else if (down_axis == 1) {
        x = 0; y = 2; z = 1;
        magSign = { 1, -1, -1 };
        accelSign = gyroSign = { 1, 1, -1 };
    }
    else {
        x = 0; y = 1; z = 2;
        magSign = { 1, 1, -1 };
        accelSign = gyroSign = { 1, 1, 1 };
    }
}

LsmConverter::LsmConverter(LsmSettings settings) :
    m_settings(settings)
{
    calcAResolution();
    calcGResolution();
    calcMResolution();

    int id = 0;
    for (int i = 1; i < 3; ++i) {
        if (abs(m_settings.accel_bias[i]) > abs(m_settings.accel_bias[id])) {
            id = i; // find axis with max accel value
        }
    }

    m_orientation.changeOrientation(id);

    if (m_settings.accel_bias[m_orientation.z] > 0) {
        m_settings.accel_bias[m_orientation.z] -= (int32_t) (1.0 / m_aResolution);
    } // Remove gravity from the z-axis accelerometer bias calculation
    else {
        m_settings.accel_bias[m_orientation.z] += (int32_t) (1.0 / m_aResolution);
    }

    previous = std::chrono::steady_clock::now();
}

LsmConverter::~LsmConverter()
{

}

CalcData LsmConverter::getCalcData() const
{
    return m_data;
}

EulerAngles LsmConverter::convertToAngles()
{
    auto timeDiff = std::chrono::steady_clock::now() - previous;
    auto sec = std::chrono::duration_cast<std::chrono::duration<float> >(timeDiff);
    deltat = sec.count();
    previous = std::chrono::steady_clock::now();

    MadgwickUpdate();
    //MahonyUpdate();

    EulerAngles angles;
    angles.yaw = atan2(2.0f * (m_q[1] * m_q[2] + m_q[0] * m_q[3]), m_q[0] * m_q[0] + m_q[1] * m_q[1] - m_q[2] * m_q[2] - m_q[3] * m_q[3]);
    angles.pitch = -asin(2.0f * (m_q[1] * m_q[3] - m_q[0] * m_q[2]));
    angles.roll  = atan2(2.0f * (m_q[0] * m_q[1] + m_q[2] * m_q[3]), m_q[0] * m_q[0] - m_q[1] * m_q[1] - m_q[2] * m_q[2] + m_q[3] * m_q[3]);

    angles.yaw   *= 180.0f / PI; 
    //yaw   -= 13.8; // Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04

    angles.roll  *= 180.0f / PI;
    angles.pitch *= 180.0f / PI;

    return angles;
}

void LsmConverter::setCompassData(std::array<int16_t, AxisDOF> & accel,
                                  std::array<int16_t, AxisDOF> & gyro,
                                  std::array<int16_t, AxisDOF> & mag)
{
    try {
        calcAccel(accel);
        calcGyro(gyro);
        calcMag(mag);
    }
    catch (const out_of_range & err) {
        cout << "Out of range while calc lsm data" << endl;
    }
}

void LsmConverter::calcAResolution()
{ 
    m_aResolution = m_settings.aScale == A_SCALE_16G ? 16.0f / 32768.0f :
        ((float)m_settings.aScale + 1.0f) * 2.0f / 32768.0f;
}

void LsmConverter::calcGResolution()
{
    switch (m_settings.gScale) {
        case G_SCALE_245DPS:
            m_gResolution = 245.0f / 32768.0f;
            break;
        case G_SCALE_500DPS:
            m_gResolution = 500.0f / 32768.0f;
            break;
        case G_SCALE_2000DPS:
            m_gResolution = 2000.0f / 32768.0f;
            break;
    }
}

void LsmConverter::calcMResolution()
{
    m_mResolution = m_settings.mScale == M_SCALE_2GS ? 2.0f / 32768.0f :
        (float)(m_settings.mScale << 2) / 32768.0f;
}

void LsmConverter::calcAccel(std::array<int16_t, AxisDOF> & accel)
{
    m_data.ax = m_orientation.accelSign.x * m_aResolution * (accel.at(m_orientation.x) - m_settings.accel_bias[m_orientation.x]);
    m_data.ay = m_orientation.accelSign.y * m_aResolution * (accel.at(m_orientation.y) - m_settings.accel_bias[m_orientation.y]);
    m_data.az = m_orientation.accelSign.z * m_aResolution * (accel.at(m_orientation.z) - m_settings.accel_bias[m_orientation.z]);
}

void LsmConverter::calcGyro(std::array<int16_t, AxisDOF> & gyro)
{
    m_data.gx = m_orientation.gyroSign.x * m_gResolution * (gyro.at(m_orientation.x) - m_settings.gyro_bias[m_orientation.x]);
    m_data.gy = m_orientation.gyroSign.y * m_gResolution * (gyro.at(m_orientation.y) - m_settings.gyro_bias[m_orientation.y]);
    m_data.gz = m_orientation.gyroSign.z * m_gResolution * (gyro.at(m_orientation.z) - m_settings.gyro_bias[m_orientation.z]);
}

void LsmConverter::calcMag(std::array<int16_t, AxisDOF> & mag)
{
    m_data.mx = m_orientation.magSign.x * m_mResolution * mag.at(m_orientation.x);
    m_data.my = m_orientation.magSign.y * m_mResolution * mag.at(m_orientation.y);
    m_data.mz = m_orientation.magSign.z * m_mResolution * mag.at(m_orientation.z);
}

void LsmConverter::MadgwickUpdate()
{
    float ax = m_data.ax, ay = m_data.ay, az = m_data.az;
    float gx = m_data.gx * PI / 180.0f, gy = m_data.gy * PI / 180.0f, gz = m_data.gz * PI / 180.0f;
    float mx = m_data.mx, my = m_data.my, mz = m_data.mz;

    float q1 = m_q[0], q2 = m_q[1], q3 = m_q[2], q4 = m_q[3];

    // Auxiliary variables to avoid repeated arithmetic
    float _2q1 = 2.0f * q1;
    float _2q2 = 2.0f * q2;
    float _2q3 = 2.0f * q3;
    float _2q4 = 2.0f * q4;
    float q1q1 = q1 * q1;
    float q1q2 = q1 * q2;
    float q1q3 = q1 * q3;
    float q1q4 = q1 * q4;
    float q2q2 = q2 * q2;
    float q2q3 = q2 * q3;
    float q2q4 = q2 * q4;
    float q3q3 = q3 * q3;
    float q3q4 = q3 * q4;
    float q4q4 = q4 * q4;

    // Normalize accelerometer measurement
    if (normalize(ax, ay, az) != 0) {
        return;
    }

    // Normalize magnetometer measurement
    if (normalize(mx, my, mz) != 0) {
        return;
    };
            
    // Reference direction of Earth's magnetic field
    float _2q1mx = 2.0f * q1 * mx;
    float _2q1my = 2.0f * q1 * my;
    float _2q1mz = 2.0f * q1 * mz;
    float _2q2mx = 2.0f * q2 * mx;
    float hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4;
    float hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4;
    float _2bx = sqrt(hx * hx + hy * hy);
    float _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4;
    float _4bx = 2.0f * _2bx;
    float _4bz = 2.0f * _2bz;
    float _8bx = 2.0f * _4bx;
    float _8bz = 2.0f * _4bz;
    
    // Gradient decent algorithm corrective step
    float s1 = -_2q3 * (2.0f *(q2q4 - q1q3) - ax) + _2q2 * (2.0f * (q1q2 + q3q4) - ay) + -_4bz * q3 * (_4bx * (0.5f - q3q3 - q4q4) + _4bz * (q2q4 - q1q3) - mx) + (-_4bx * q4 + _4bz * q2) * (_4bx * (q2q3 - q1q4) + _4bz * (q1q2 + q3q4) - my) + _4bx * q3 * (_4bx * (q1q3 + q2q4) + _4bz * (0.5f - q2q2 - q3q3) - mz);
    float s2 = _2q4 * (2.0f * (q2q4 - q1q3) - ax) + _2q1 * (2.0f * (q1q2 + q3q4) - ay) + -4.0f * q2 * (2.0f * (0.5f - q2q2 - q3q3) - az) + _4bz * q4 * (_4bx * (0.5f - q3q3 - q4q4) + _4bz * (q2q4 - q1q3) - mx) + (_4bx * q3 + _4bz * q1) * (_4bx * (q2q3 - q1q4) + _4bz * (q1q2 + q3q4) - my) + (_4bx * q4 - _8bz * q2) * (_4bx * (q1q3 + q2q4) + _4bz * (0.5f - q2q2 - q3q3) - mz);    
    float s3 = -_2q1 * (2.0f * (q2q4 - q1q3) - ax) + _2q4 * (2.0f * (q1q2 + q3q4) - ay) + (-4.0f * q3) * (2.0f * (0.5f - q2q2 - q3q3) - az) + (-_8bx * q3 - _4bz * q1) * (_4bx * (0.5f - q3q3 - q4q4) + _4bz * (q2q4 - q1q3) - mx) + (_4bx * q2 + _4bz * q4) * (_4bx * (q2q3 - q1q4) + _4bz * (q1q2 + q3q4) - my) + (_4bx * q1 - _8bz * q3) * (_4bx * (q1q3 + q2q4) + _4bz * (0.5f - q2q2 - q3q3) - mz);    
    float s4 = _2q2 * (2.0f * (q2q4 - q1q3) - ax) + _2q3 * (2.0f * (q1q2 + q3q4) - ay) + (-_8bx * q4 + _4bz * q2) * (_4bx * (0.5f - q3q3 - q4q4) + _4bz * (q2q4 - q1q3) - mx) + (-_4bx * q1 + _4bz * q3) * (_4bx * (q2q3 - q1q4) + _4bz * (q1q2 + q3q4) - my) + (_4bx * q2) * (_4bx * (q1q3 + q2q4) + _4bz * (0.5f - q2q2 - q3q3) - mz);
    
    float norm = sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
    s1 /= norm;
    s2 /= norm;
    s3 /= norm;
    s4 /= norm;

    // Compute rate of change of quaternion
    float qDot1 = 0.5f * (-q2 * gx - q3 * gy - q4 * gz) - beta * s1;
    float qDot2 = 0.5f * (q1 * gx + q3 * gz - q4 * gy) - beta * s2;
    float qDot3 = 0.5f * (q1 * gy - q2 * gz + q4 * gx) - beta * s3;
    float qDot4 = 0.5f * (q1 * gz + q2 * gy - q3 * gx) - beta * s4;

    // Integrate to yield quaternion
    q1 += qDot1 * deltat;
    q2 += qDot2 * deltat;
    q3 += qDot3 * deltat;
    q4 += qDot4 * deltat;
    norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalize quaternion
    m_q[0] = q1 / norm;
    m_q[1] = q2 / norm;
    m_q[2] = q3 / norm;
    m_q[3] = q4 / norm;
}

int LsmConverter::normalize(float &x, float &y, float &z)
{
    if (x == 0.0f && y == 0.0f && z == 0.0f) {
        return -1;
    }

    float normValue = sqrt(x * x + y * y + z * z);

    x /= normValue;
    y /= normValue;
    z /= normValue;

    return 0;
}

void LsmConverter::MahonyUpdate() 
{
    float q1 = m_q[0], q2 = m_q[1], q3 = m_q[2], q4 = m_q[3];

    float ax = m_data.ax, ay = m_data.ay, az = m_data.az;
    float gx = m_data.gx * PI / 180.0f, gy = m_data.gy * PI / 180.0f, gz = m_data.gz * PI / 180.0f;
    float mx = m_data.mx, my = m_data.my, mz = m_data.mz;

    float q1q1 = q1 * q1;
    float q1q2 = q1 * q2;
    float q1q3 = q1 * q3;
    float q1q4 = q1 * q4;
    float q2q2 = q2 * q2;
    float q2q3 = q2 * q3;
    float q2q4 = q2 * q4;
    float q3q3 = q3 * q3;
    float q3q4 = q3 * q4;
    float q4q4 = q4 * q4;   

    if (normalize(ax, ay, az) != 0) {
        return;
    }

    if (normalize(mx, my, mz) != 0) {
        return;
    }
    
    // Reference direction of Earth's magnetic field
    float hx = 2.0f * mx * (0.5f - q3q3 - q4q4) + 2.0f * my * (q2q3 - q1q4) + 2.0f * mz * (q2q4 + q1q3);
    float hy = 2.0f * mx * (q2q3 + q1q4) + 2.0f * my * (0.5f - q2q2 - q4q4) + 2.0f * mz * (q3q4 - q1q2);
    float bx = sqrt((hx * hx) + (hy * hy));
    float bz = 2.0f * mx * (q2q4 - q1q3) + 2.0f * my * (q3q4 + q1q2) + 2.0f * mz * (0.5f - q2q2 - q3q3);

    // Estimated direction of gravity and magnetic field
    float vx = 2.0f * (q2q4 - q1q3);
    float vy = 2.0f * (q1q2 + q3q4);
    float vz = q1q1 - q2q2 - q3q3 + q4q4;
    float wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
    float wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
    float wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);  

    // Error is cross product between estimated direction and measured direction of gravity
    float ex = (ay * vz - az * vy) + (my * wz - mz * wy);
    float ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
    float ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
    if (Ki > 0.0f) { // accumulate integral error
        eInt[0] += ex;
        eInt[1] += ey;
        eInt[2] += ez;
    } 
    else {
        for (int i = 0; i < 3; ++i) {
            eInt[i] = 0.0f;
        }
    }

    // Apply feedback terms
    gx = gx + Kp * ex + Ki * eInt[0];
    gy = gy + Kp * ey + Ki * eInt[1];
    gz = gz + Kp * ez + Ki * eInt[2];

    // Integrate rate of change of quaternion
    float pa = q2;
    float pb = q3;
    float pc = q4;
    q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * deltat);
    q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * deltat);
    q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * deltat);
    q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * deltat);

    float norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalize quaternion
    m_q[0] = q1 / norm;
    m_q[1] = q2 / norm;
    m_q[2] = q3 / norm;
    m_q[3] = q4 / norm;
}