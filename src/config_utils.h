#pragma once

#include <string>
#include <iostream>
#include <libconfig.h++>

class ConfigReader
{
public:
    static ConfigReader& instance();

    ConfigReader(ConfigReader&&) = delete;
    ConfigReader(ConfigReader const &) = delete;

    ConfigReader& operator=(ConfigReader&&) = delete;
    ConfigReader& operator=(ConfigReader const &) = delete;

    template <typename T>
    T getValue(const std::string& path) const
    {
        T value;
        try {
            value = m_cfg.lookup(path);
            std::cout << path << " : " << value << std::endl;
        }
        catch (const libconfig::SettingNotFoundException) {
            std::cout << "No " << path << " setting in config file" << std::endl;
        }

        return value;
    }

protected:
    ConfigReader();
    ~ConfigReader();

private:
    libconfig::Config m_cfg;
    std::string m_configFileName = "/home/root/murbin/config/server.cfg";
};

template <>
inline std::string ConfigReader::getValue<std::string>(const std::string& path) const
{
    std::string value;
    try {
        value = m_cfg.lookup(path).c_str();
        std::cout << path << " : " << value << std::endl;
    }
    catch (const libconfig::SettingNotFoundException) {
        std::cout << "No " << path << " setting in config file" << std::endl;
    }

    return value;
}

#define REG_CONFIG_VALUE(type, name) \
    type name = ConfigReader::instance().getValue<type>(#name);
