#include "time_utils.h"

#include <ctime>
#include <sys/time.h>

void printTime()
{
    timeval tp;
    gettimeofday(&tp, 0);
    time_t timePoint = tp.tv_sec; 
    tm* t = localtime(&timePoint);
    printf("%02d:%02d:%02d:%03d ", t->tm_hour, t->tm_min, t->tm_sec, (int)(tp.tv_usec / 1000));
}
