#include "server.h"
#include "collect_mag_values.h"

#include <thread>
#include <iostream>
#include <exception>

using namespace std;

int main(int argc, char* argv[])
{
    bool collectMagData = false;

    for (int i = 0; i < argc; ++i) {
        string option = string(argv[i]);
        if (option == "-c" || option == "--collect_mag") {
            collectMagData = true;
            break;  
        }
        if (option == "-h" || option == "--help") {
            cout << "Usage for " << argv[0] << ": " << endl;
            cout << "<no params> - server mode" << endl;
            cout << "-c or --collect_mag - Collect raw magnetometer data" << endl;
            return 0;
        }
    }

    if (collectMagData) {
        MagDataCollector c;
        c.collect();
    }
    else {
        try {
        Server::instance().start();
        }
        catch (const exception &e) {
            cout << "Error while create new server instance" << endl;
            cout << e.what() << endl;
            return 1;
        }

        while (true) {
            Server::instance().update();
            this_thread::sleep_for(chrono::milliseconds(1));
        }    
    }
    
    return 0;
}