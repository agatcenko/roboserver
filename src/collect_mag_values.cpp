#include "collect_mag_values.h"
#include "timer.h"

#include <chrono>
#include <thread>
#include <limits>
#include <iostream>

using namespace std;

MagDataCollector::MagDataCollector() :
    m_rawMagFile(m_rawMagFileName.c_str()),
    m_extremumMagFile(m_extremumMagFileName.c_str()),
    m_lsmController(unique_ptr<LsmController>(new LsmController(m_lsmBus)))
{
    m_magMin.fill(numeric_limits<short>::max());
    m_magMax.fill(numeric_limits<short>::min());
}

MagDataCollector::~MagDataCollector()
{
    if (m_rawMagFile.is_open()) {
        m_rawMagFile.close();    
    }
    if (m_extremumMagFile.is_open()) {
        m_extremumMagFile.close();
    }
}

void MagDataCollector::collect()
{
    if (!m_rawMagFile.is_open() || !m_extremumMagFile.is_open()) {
        cout << "Output files are closed." << endl;
        return;
    }

    OneShotTimer timer(m_captureDuration);
    timer.start();

    try {
        while (!timer.isCompleted()) {
            MagValues magVal = m_lsmController->readMag();

            for (auto i = 0; i < AXES_NUM; ++i) {
                if (magVal.at(i) < m_magMin.at(i)) {
                    m_magMin.at(i) = magVal.at(i);
                }

                if (magVal.at(i) > m_magMax.at(i)) {
                    m_magMax.at(i) = magVal.at(i);
                }
            }

            for (const auto & val : magVal) {
                m_rawMagFile << val << "\t";
            }
            m_rawMagFile << "\r\n";

            this_thread::sleep_for(chrono::milliseconds(m_captureTimeout));
        }

        for (auto i = 0; i < AXES_NUM; ++i) {
            m_extremumMagFile << m_magMin.at(i) << "\r\n" << m_magMax.at(i) << "\r\n";
        }    
    }
    catch (const out_of_range & err) {
        cout << "Out of range while calc lsm data" << endl;
    }
    catch (const exception & err) {
        cout << "Exception: " << err.what() << endl;
    }   
}