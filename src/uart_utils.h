#pragma once

#include "mraa.hpp"
#include "timer.h"
#include "msg_types.h"
#include "config_utils.h"
#include "uart_reply_storage.h"
#include "uart_request_storage.h"

#include <map>
#include <array>
#include <queue>
#include <vector>
#include <string>
#include <memory>
#include <exception>
#include <functional>

//! Enum with types of confirmations
enum class PackageConfirmation : uint8_t
{
    WITHOUT_CONFIRM = 0x00, /*!< No need to answer on message */
    WITH_CONFIRM = 0x0A,    /*!< Need to answer on message    */
    IS_REPLY = 0x0B         /*!< This message is an answer    */
};

/*!
    \brief Class with uart communication implementatiion
    
    Get available output messages from UartRequestStorage instance and send them.
    Read data from serial port, decode and save it with UartReplyStorage instance.
*/
class UartManager {
public:
    //! UartManager constructor
    /*!
        \param [in] device string representation of serial port device
        \param [in, out] replyStorage reference for UartReplyStorage instance
        \param [in, out] requestStorage reference for UartRequestStorage instance
    */
    UartManager(std::string device, UartReplyStorage &replyStorage, UartRequestStorage &requestStorage);
    //! UartManager destructor
    /*! Free serial device */
    ~UartManager();

    void getDeviceType(); //!< Send package with device type request
    void getSupervisorInfo(); //!< Send package with supervisor data request
    void getAltimeterInfo(); //!< Send package with altimeter data request
    //! Send package with thruster data request
    /*! \param [in] id id number of thruster*/
    void getThrusterStatus(uint8_t id);

    void sendMissionStatus(); //!< Send package with new mission status
    void sendThrustersPower(); //!< Send package with new thrusters power
    void updateDevicesStatus(); //!< Put in queue new device status request

    bool isReady() const; //!< Check if channel is ready to send new package
    void execRequest(); //!< Send new request
    bool isNoRequests() const; //!< Check if there are any requests in queue

private:
    mraa::Uart* m_dev; //!< Mraa object for manipulate serial device

    uint8_t m_isButtonProcessed = 0; //!< Status of button
    
    std::vector<uint8_t> m_receiveBuffer; //!< Buffer for input raw data
    //! Map for matching function for sending request by its type
    std::map<RequestType, void (UartManager::*)()> m_requestHandlers;
    //! Map for mathing function for handle input package by its type 
    std::map<uint8_t, void (UartManager::*)(const std::vector<uint8_t>&)> m_msgHandlers;

    //! Timer used for measure time between two consequent output packages
    std::shared_ptr<OneShotTimer> m_silenceTimer;

    UartReplyStorage& m_uartReplyStorage; //!< Reference for UartReplyStorage instance
    UartRequestStorage& m_uartRequestStorage; //!< Reference for UartRequestStorage instance

    AdcInfo m_supervisorInfo; //!< Store input data from supervisor microcontoller

    size_t currentDeviceIt = 0; //!< Counter used for get next device id
    std::array<uint8_t, DEVICES_NUM> m_devicesAddr; //!< Array with ids of connected devices
    std::array<uint8_t, DEVICES_NUM> m_devicesStatus; //!< Array with types of connected devices

    //!< Configure serial port
    /*! \return operation status */
    int setUartSettings();
    
    void getResponse(); //!< Read input data
    
    //! Build output package from params and send it
    /*!
        \param [in] msgId package type
        \param [in] receiverId address of destination device 
        \param [in] confirmFlag flag of confirmation type
        \param [in] data usefull data (package body)
        \return send status
     */
    int sendPackage(uint8_t msgId, uint8_t receiverId,
                    PackageConfirmation confirmFlag, std::vector<uint8_t> &data);

    //! Extract usefull data from input package
    /*! \param [in] package raw input data */
    void processInputPackage(std::vector<uint8_t> &package);

    //! Add entry in m_requestHandlers map
    /*!
        \param [in] type type of request message
        \param [in] handler pointer to class member function that send such request
    */
    void registerRequestHandler(RequestType type, void (UartManager::*handler)());
    
    //! Add entry in m_msgHandlers map
    /*!
        \param [in] msgId type of input message
        \param [in] handler pointer to class member function that decode input message
    */
    void registerIncomeMsg(uint8_t msgId, void (UartManager::*handler)(const std::vector<uint8_t>&));

    //! Update CRC value for new input byte
    /*!
        \param [in] acc accumulator, store CRC value
        \param [in] input new data byte
        \return updated crc value
    */
    uint16_t updateCRC(uint16_t acc, const uint8_t input);
    
    //! Calculate CRC value
    /*!
        \param [in] data source data
        \param [in] len length of source data
        \return calculated crc
    */
    uint16_t calculateCRC(const uint8_t *data, const int32_t len);

    // income msg handlers
    //! Handler that process input data when error occurs
    /*! \param [in] package body */
    void errorHandler(const std::vector<uint8_t> &data);
    //! Handler that process input data for DeviceType package
    /*! \param [in] package body */
    void deviceTypeHandler(const std::vector<uint8_t> &data);
    //! Handler that process input data for ThrusterStatus package
    /*! \param [in] package body */
    void thrusterStatusHandler(const std::vector<uint8_t> &data);
    //! Handler that process input data for SupervisorData package
    /*! \param [in] package body */
    void supervisorDataHandler(const std::vector<uint8_t> &data);
    //! Handler that process input data for altimeterData package
    /*! \param [in] package body */
    void altimeterDataHandler(const std::vector<uint8_t> &data);

    // Config values (see uart_utils.cfg)

    REG_CONFIG_VALUE(unsigned int, m_silenceTimeout) //!< Time interval in ms between output packages

    // CRC16 constants
    REG_CONFIG_VALUE(int, SEED) //!< Init value in CRC16 algo
    REG_CONFIG_VALUE(int, POLY) //!< CRC16 polynominal representation

    // Uart settings
    REG_CONFIG_VALUE(bool, RTS_CTS) //!< Hardware flow control
    REG_CONFIG_VALUE(bool, XON_XOFF) //!< Software flow control
    REG_CONFIG_VALUE(int, BYTE_SIZE) //!< Data bits count
    REG_CONFIG_VALUE(int, STOP_BITS) //!< Stop bits count
    REG_CONFIG_VALUE(unsigned int, BAUDRATE) //<! Baudrate value

    // Devices id
    REG_CONFIG_VALUE(unsigned int, BROADCAST) //!< Id for broadcast mode
    REG_CONFIG_VALUE(unsigned int, EDISON_ID) //!< Id of edison computer
    REG_CONFIG_VALUE(unsigned int, ALTIMETER_ID) //!< Id of altimeter device
    REG_CONFIG_VALUE(unsigned int, SUPERVISOR_ID) //!< Id of supervisor microcontroller

    // Package bytes location
    REG_CONFIG_VALUE(unsigned int, MSG_ID_NUM) //!< Location of package id byte
    REG_CONFIG_VALUE(unsigned int, PACKAGE_SIZE_NUM) //!< Location of package size byte
    REG_CONFIG_VALUE(unsigned int, CONFIRM_FLAG_NUM) //!< Location of package confirmation flag
    REG_CONFIG_VALUE(unsigned int, RECEIVER_BYTE_NUM) //!< Location of package receiver id byte
    REG_CONFIG_VALUE(unsigned int, START_PACKAGE_NUM_1) //!< Location of first preamble byte in package
    REG_CONFIG_VALUE(unsigned int, START_PACKAGE_NUM_2) //!< Location of second preamble byte in package
    REG_CONFIG_VALUE(unsigned int, TRANSMITTER_BYTE_NUM) //!< Location of package transmitter id byte

    // package bytes values
    REG_CONFIG_VALUE(unsigned int, END_PACKAGE_VAL) //!< Value of epilogue byte in package
    REG_CONFIG_VALUE(unsigned int, START_PACKAGE_VAL_1) //!< Value of first preamble byte in package
    REG_CONFIG_VALUE(unsigned int, START_PACKAGE_VAL_2) //!< Value of second preamble byte in package

    //! Size of service information (preamble, crc, ids, ...) in package
    REG_CONFIG_VALUE(unsigned int, SERVICE_INFO_SIZE)

    //! Offset for pdu (package data unit)
    REG_CONFIG_VALUE(unsigned int, DATA_OFFSET)

    // Output msgs id
    REG_CONFIG_VALUE(unsigned int, CONTROL_THRUSTER) //!< Id for ControlThruster package
    REG_CONFIG_VALUE(unsigned int, CONTROL_LED_MODE) //!< Id for ControlLedMode package
    REG_CONFIG_VALUE(unsigned int, REQUEST_DEVICE_TYPE) //!< Id for RequestDeviceType package
    REG_CONFIG_VALUE(unsigned int, REQUEST_THRUSTER_STATUS) //!< Id for RequestThrusterStatus package
    REG_CONFIG_VALUE(unsigned int, REQUEST_SUPERVISOR_DATA) //!< Id for RequestSupervisorStatus package
    REG_CONFIG_VALUE(unsigned int, REQUEST_ALTIMETER_DATA) //!< Id for RequestAltimeterStatus package
    REG_CONFIG_VALUE(unsigned int, CONTROL_THRUSTERS_BROADCAST) //!< Id for ControlThrustersBroadcast package

    // Input msgs id
    REG_CONFIG_VALUE(unsigned int, ERROR_MSG) //!< Id for Error package
    REG_CONFIG_VALUE(unsigned int, DEVICE_TYPE) //!< Id for DeviceType package
    REG_CONFIG_VALUE(unsigned int, THRUSTER_STATUS) //!< Id for ThrusterStatus package
    REG_CONFIG_VALUE(unsigned int, SUPERVISOR_DATA) //!< Id for SupervisorData package
    REG_CONFIG_VALUE(unsigned int, ALTIMETER_DATA) //!< Id for AltimeterData package

    //! Time delay for uart transfer in milliseconds 
    REG_CONFIG_VALUE(unsigned int, RESPONSE_WAIT)

    REG_CONFIG_VALUE(unsigned int, UNDEF_ADDR) //!< Value of undefined device address
    REG_CONFIG_VALUE(unsigned int, UNDEF_DEVICE) //!< Value of undefined device type

    //! Offset for peripheral type data
    REG_CONFIG_VALUE(unsigned int, DEVICE_TYPE_OFFSET)

    // Offsets in thruster status msg
    REG_CONFIG_VALUE(unsigned int, RPM_OFFSET_H) //!< Location of RotatePerMinute high byte
    REG_CONFIG_VALUE(unsigned int, RPM_OFFSET_L) //!< Location of RotatePerMinute low byte
    REG_CONFIG_VALUE(unsigned int, POWER_OFFSET) //!< Location of Power byte
    REG_CONFIG_VALUE(unsigned int, CURRENT_OFFSET_H) //!< Location of Current high byte
    REG_CONFIG_VALUE(unsigned int, CURRENT_OFFSET_L) //!< Location of Current low byte

    // Offsets in supervisor data msg
    REG_CONFIG_VALUE(unsigned int, TEMP_OFFSET_H) //!< Location of Temperature high byte
    REG_CONFIG_VALUE(unsigned int, TEMP_OFFSET_L) //!< Location of Temperature low byte
    REG_CONFIG_VALUE(unsigned int, DEPTH_OFFSET_H) //!< Location of Depth high byte
    REG_CONFIG_VALUE(unsigned int, DEPTH_OFFSET_L) //!< Location of Depth low byte
    REG_CONFIG_VALUE(unsigned int, BATTERY_OFFSET_H) //!< Location of BatteryVoltage high byte
    REG_CONFIG_VALUE(unsigned int, BATTERY_OFFSET_L) //!< Location of BatteryVoltage low byte
    REG_CONFIG_VALUE(unsigned int, BUTTON_LEAK_OFFSET) //!< Location of ButtonLeak byte

    // Offsets in altimeter data msg
    REG_CONFIG_VALUE(unsigned int, ALTITUDE_OFFSET_H) //!< Location of Altitude high byte
    REG_CONFIG_VALUE(unsigned int, ALTITUDE_OFFSET_L) //!< Location of Altitude low byte
};