#pragma once

#include <chrono>

/*! \file
    \brief Header file with timers description

    Holds description of different types of timers.
*/

/*!
    \brief Class for simple timer

    Auto reset timer.  
    Reset if timeout is exceeded and programm checked timer status using isCompleted function.
*/

class Timer
{
public:
    //! A constructor
    /*!
        \param timeout Timeout value in milliseconds 
    */
    Timer(unsigned int timeout);
    //! A destructor
    virtual ~Timer();
    
    //! Activate timer
    void start();
    //! Reset timer
    /*!
        Time counting begins anew. Set current time to variable m_start. 
    */
    void reset();
    //! Check the timer status
    /*!
        Calculate difference between current time point and start time point.
        If the difference is greater than timeout value, then reset timer.  
        \return Timer status
    */
    virtual bool isCompleted();

protected:
    unsigned int m_timeout; //!< Timout value in milliseconds
    std::chrono::time_point<std::chrono::steady_clock> m_start; //!< Start time point
};

/*!
    \brief Class for one shot timer
    
    Timer that does not support auto reset.
*/
class OneShotTimer : public Timer
{
public:
    //! A constructor
    /*!
        \param timeout Timeout value in milliseconds 
    */
    OneShotTimer(unsigned int timeout) : Timer(timeout) {}
    //! A destructor
    ~OneShotTimer() {}

    //! Check the timer status
    /*!
        Calculate difference between current time point and start time point.
        \return Timer status
    */
    bool isCompleted();
};
