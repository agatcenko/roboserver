#pragma once

#include "lsm_types.h"
#include "config_utils.h"

#include "math.h"
#include <array>
#include <chrono>

struct EulerAngles
{
    float yaw;
    float roll;
    float pitch;
};

struct CalcData
{
    float ax;
    float ay;
    float az;
    float mx;
    float my;
    float mz;
    float gx;
    float gy;
    float gz;
};

struct AxesSigns
{
    int x, y, z;

    AxesSigns(int _x = 1, int _y = 1, int _z = 1)
    {
        // Convert input to 1 or -1
        x = _x / abs(_x);
        y = _y / abs(_y);
        z = _z / abs(_z);
    }
};

struct Orientation
{
    int x, y, z;

    AxesSigns magSign;
    AxesSigns gyroSign;
    AxesSigns accelSign;

    Orientation(int _x = 0, int _y = 1, int _z = 2) :
        x(_x), y(_y), z(_z) {}

    void changeOrientation(int down_axis);
};

class LsmConverter
{
public:
    LsmConverter(LsmSettings settings);
    ~LsmConverter();

    CalcData getCalcData() const;
    EulerAngles convertToAngles();
    
    void setCompassData(std::array<int16_t, AxisDOF> & accel,
                        std::array<int16_t, AxisDOF> & gyro,
                        std::array<int16_t, AxisDOF> & mag);

private:
    CalcData m_data;
    LsmSettings m_settings;

    float m_q[4] = { 1.0f, 0.0f, 0.0f, 0.0f }; // quaternion
    float eInt[3] ={ 0.0f, 0.0f, 0.0f };
    float g_bias[3] = { 0.0f, 0.0f, 0.0f };
    float a_bias[3] = { 0.0f, 0.0f, 0.0f };

    float deltat;
    std::chrono::time_point<std::chrono::steady_clock> previous;

    float m_aResolution;
    float m_gResolution;
    float m_mResolution;

    Orientation m_orientation;

    void calcAccel(std::array<int16_t, AxisDOF> & accel);
    void calcGyro(std::array<int16_t, AxisDOF> & gyro);
    void calcMag(std::array<int16_t, AxisDOF> & mag);

    void calcAResolution();
    void calcGResolution();
    void calcMResolution();

    void MadgwickUpdate();
    void MahonyUpdate();

    int normalize(float &x, float &y, float &z);

    REG_CONFIG_VALUE(float, PI)
    REG_CONFIG_VALUE(float, Kp)
    REG_CONFIG_VALUE(float, Ki)
    REG_CONFIG_VALUE(float, beta)
    REG_CONFIG_VALUE(float, GyroMeasError)
};