#include "lsm_controller.h"

#include <thread>
#include <chrono>
#include <memory>
#include <fstream>
#include <iostream>

using namespace std;
using namespace mraa;

#define LSM_BUS 1

#define G_DEFAULT_ADDRESS 0x6A
#define XM_DEFAULT_ADDRESS 0x1E

LsmController::LsmController(int bus)
{
    m_i2c = unique_ptr<I2c>(new I2c(bus));

    setFrequency(I2C_HIGH);
    setAddresses(G_DEFAULT_ADDRESS, XM_DEFAULT_ADDRESS);

    m_lsmSettings = {
        G_SCALE_245DPS,
        A_SCALE_2G,
        M_SCALE_2GS,
        G_ODR_95_BW_125,
        A_ODR_50,
        M_ODR_50,
        A_ABW_50
    };

    // wait for compass initialization
    this_thread::sleep_for(chrono::seconds(1));

    initLsm();
    calcBiases();
    loadMagCalibrationData();
}

void LsmController::setFrequency(I2cMode frequency)
{
    m_frequency = frequency;

    m_i2c->frequency(m_frequency);
}

void LsmController::setAddresses(uint8_t gAddress, uint8_t xmAddress)
{
    m_gAddress = gAddress;
    m_xmAddress = xmAddress;
}

LsmSettings LsmController::getSettings() const
{
    return m_lsmSettings;
}

uint8_t LsmController::readReg(uint8_t i2cAddress, uint8_t regAddress) const
{
    Result res = m_i2c->address(i2cAddress);
    uint8_t receiveByte = 0;
    if (res == SUCCESS) {
        receiveByte = m_i2c->readReg(regAddress);
    }

    return receiveByte;
}

uint8_t LsmController::gReadReg(uint8_t regAddress) const
{
    return readReg(m_gAddress, regAddress);
}

uint8_t LsmController::xmReadReg(uint8_t regAddress) const
{
    return readReg(m_xmAddress, regAddress);
}

void LsmController::readRegs(uint8_t i2cAddress, uint8_t regAddress, 
                             uint8_t* dest, uint8_t len) const
{
    Result res = m_i2c->address(i2cAddress);
    if (res == SUCCESS) {
        regAddress |= 0x80;
        m_i2c->readBytesReg(regAddress, dest, len);
    }
}

void LsmController::writeReg(uint8_t i2cAddress, uint8_t regAddress, uint8_t data) const
{
    Result res = m_i2c->address(i2cAddress);
    if (res == SUCCESS) {
        Result writeRes = m_i2c->writeReg(regAddress, data);
        if (writeRes != SUCCESS) {
            cout << "Error while write to register" << endl;
        }
    }
}

void LsmController::gWriteReg(uint8_t regAddress, uint8_t data) const
{
    writeReg(m_gAddress, regAddress, data);
}

void LsmController::xmWriteReg(uint8_t regAddress, uint8_t data) const
{
    writeReg(m_xmAddress, regAddress, data);
}

void LsmController::initLsm() const
{
    initGyro();
    setGyroODR();
    setGyroScale();
  
    initAccel();
    setAccelABW();
    setAccelODR();
    setAccelScale();
  
    initMag();
    setMagODR();
    setMagScale();
}

void LsmController::calcBiases()
{
    uint8_t data[6] = { 0, 0, 0, 0, 0, 0 };
    
    for (int i = 0; i < 3; ++i) {
        m_lsmSettings.gyro_bias[i] = 0;
        m_lsmSettings.accel_bias[i] = 0;
    }

    uint8_t c = gReadReg(CTRL_REG5_G);
    gWriteReg(CTRL_REG5_G, c | 0x40);
    this_thread::sleep_for(chrono::milliseconds(20));
    gWriteReg(FIFO_CTRL_REG_G, 0x40 | 0x1F);  // Enable gyro FIFO stream mode and set watermark at 32 samples
    this_thread::sleep_for(chrono::milliseconds(1000));  // delay 1000 milliseconds to collect FIFO samples
  
    uint8_t samples = (gReadReg(FIFO_SRC_REG_G) & 0x1F); // Read number of stored samples
    for (uint8_t i = 0; i < samples; ++i) { // Read the gyro data stored in the FIFO
        int16_t gyro_temp[3] = { 0, 0, 0 };
        readRegs(m_gAddress, OUT_X_L_G, data, 6);
        gyro_temp[0] = (int16_t) (((int16_t)data[1] << 8) | data[0]); // Form signed 16-bit integer for each sample in FIFO
        gyro_temp[1] = (int16_t) (((int16_t)data[3] << 8) | data[2]);
        gyro_temp[2] = (int16_t) (((int16_t)data[5] << 8) | data[4]);

        m_lsmSettings.gyro_bias[0] += (int32_t) gyro_temp[0]; // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
        m_lsmSettings.gyro_bias[1] += (int32_t) gyro_temp[1]; 
        m_lsmSettings.gyro_bias[2] += (int32_t) gyro_temp[2]; 
    }

    if (samples) {
        m_lsmSettings.gyro_bias[0] /= samples; // average the data
        m_lsmSettings.gyro_bias[1] /= samples; 
        m_lsmSettings.gyro_bias[2] /= samples; 
    }
  
    c = gReadReg(CTRL_REG5_G);
    gWriteReg(CTRL_REG5_G, c & ~0x40);  // Disable gyro FIFO  
    this_thread::sleep_for(chrono::milliseconds(20));
    gWriteReg(FIFO_CTRL_REG_G, 0x00);   // Enable gyro bypass mode
  
    //  Now get the accelerometer biases
    c = xmReadReg(CTRL_REG0_XM);
    xmWriteReg(CTRL_REG0_XM, c | 0x40);      // Enable accelerometer FIFO  
    this_thread::sleep_for(chrono::milliseconds(20));  // Wait for change to take effect
    xmWriteReg(FIFO_CTRL_REG, 0x40 | 0x1F);  // Enable accelerometer FIFO stream mode and set watermark at 32 samples
    this_thread::sleep_for(chrono::milliseconds(1000));  // delay 1000 milliseconds to collect FIFO samples

    samples = (xmReadReg(FIFO_SRC_REG) & 0x1F); // Read number of stored accelerometer samples

    for (uint8_t i = 0; i < samples; ++i) {          // Read the accelerometer data stored in the FIFO
        int16_t accel_temp[3] = {0, 0, 0};
        readRegs(m_xmAddress, OUT_X_L_A, data, 6);
        accel_temp[0] = (int16_t) (((int16_t)data[1] << 8) | data[0]);// Form signed 16-bit integer for each sample in FIFO
        accel_temp[1] = (int16_t) (((int16_t)data[3] << 8) | data[2]);
        accel_temp[2] = (int16_t) (((int16_t)data[5] << 8) | data[4]);  

        m_lsmSettings.accel_bias[0] += (int32_t) accel_temp[0]; // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
        m_lsmSettings.accel_bias[1] += (int32_t) accel_temp[1]; 
        m_lsmSettings.accel_bias[2] += (int32_t) accel_temp[2]; 
    }  

    if (samples) {
        m_lsmSettings.accel_bias[0] /= samples; // average the data
        m_lsmSettings.accel_bias[1] /= samples;
        m_lsmSettings.accel_bias[2] /= samples;
    }
 
    c = xmReadReg(CTRL_REG0_XM);
    xmWriteReg(CTRL_REG0_XM, c & ~0x40);    // Disable accelerometer FIFO  
    this_thread::sleep_for(chrono::milliseconds(20));  // Wait for change to take effect
    xmWriteReg(FIFO_CTRL_REG, 0x00);       // Enable accelerometer bypass mode
}

void LsmController::loadMagCalibrationData()
{
    ifstream in (m_magCalibrationFileName.c_str(), ifstream::in);
    if (!in.is_open()) {
        cout << "File with magnetometer transformation is not opened" << endl;
        return;
    }

    for (auto i = 0; i < AxisDOF; ++i) {
        in >> m_magCalibration.bias[i];
    }

    for (auto i = 0; i < AxisDOF; ++i) {
        for (auto j = 0; j < AxisDOF; ++j) {
            in >> m_magCalibration.transform[i][j];
        }
    }

    m_calibrateMag = true;
    in.close();
}

void LsmController::initMag() const
{
    xmWriteReg(CTRL_REG5_XM, 0x94); // Mag data rate - 100 Hz, enable temperature sensor
    xmWriteReg(CTRL_REG6_XM, 0x00); // Mag scale to +/- 2GS
    xmWriteReg(CTRL_REG7_XM, 0x00); // Continuous conversion mode
    xmWriteReg(CTRL_REG4_XM, 0x04); // Magnetometer data ready on INT2_XM (0x08)
    //xmWriteReg(lsm_t,INT_CTRL_REG_M, 0x09); // Enable interrupts for mag, active-low, push-pull
}

void LsmController::initGyro() const
{
    gWriteReg(CTRL_REG1_G, 0x0F); // Normal mode, enable all axes
    gWriteReg(CTRL_REG2_G, 0x00); // Normal mode, high cutoff frequency
    gWriteReg(CTRL_REG3_G, 0x88); 
    gWriteReg(CTRL_REG4_G, 0x00); // Set scale to 245 dps
    gWriteReg(CTRL_REG5_G, 0x00);
}

void LsmController::initAccel() const
{
    xmWriteReg(CTRL_REG0_XM, 0x00);
    xmWriteReg(CTRL_REG1_XM, 0x57); // 100Hz data rate, x/y/z all enabled
    xmWriteReg(CTRL_REG2_XM, 0x00); // Set scale to 2g
    xmWriteReg(CTRL_REG3_XM, 0x04);
}

void LsmController::setMagScale() const
{
    uint8_t temp = xmReadReg(CTRL_REG6_XM);
    temp &= 0xFF ^ (0x3 << 5);
    temp |= m_lsmSettings.mScale << 5;
    xmWriteReg(CTRL_REG6_XM, temp);
}

void LsmController::setGyroScale() const
{
    uint8_t temp = gReadReg(CTRL_REG4_G);
    temp &= 0xFF ^ (0x3 << 4);
    temp |= m_lsmSettings.gScale << 4;
    gWriteReg(CTRL_REG4_G, temp);
}

void LsmController::setAccelScale() const
{
    uint8_t temp = xmReadReg(CTRL_REG2_XM);
    temp &= 0xFF ^ (0x3 << 3);
    temp |= m_lsmSettings.aScale << 3;
    xmWriteReg(CTRL_REG2_XM, temp);
}

void LsmController::setMagODR() const
{
    uint8_t temp = xmReadReg(CTRL_REG5_XM);
    temp &= 0xFF ^ (0x7 << 2);
    temp |= m_lsmSettings.mODR << 2;
    xmWriteReg(CTRL_REG5_XM, temp);
}

void LsmController::setGyroODR() const
{
    uint8_t temp = gReadReg(CTRL_REG1_G);
    temp &= 0xFF ^ (0xF << 4);
    temp |= m_lsmSettings.gODR << 4;
    gWriteReg(CTRL_REG1_G, temp);
}

void LsmController::setAccelODR() const
{
    uint8_t temp = xmReadReg(CTRL_REG1_XM);
    temp &= 0xFF ^ (0xF << 4);
    temp |= m_lsmSettings.aODR << 4;
    xmWriteReg(CTRL_REG1_XM, temp);
}

void LsmController::setAccelABW() const
{
    uint8_t temp = xmReadReg(CTRL_REG2_XM);
    temp &= 0xFF ^ (0x3 << 6);
    temp |= m_lsmSettings.aABW << 6;
    xmWriteReg(CTRL_REG2_XM, temp);
}

int16_t LsmController::readTemperature() const
{
    uint8_t temp[2];
    readRegs(m_xmAddress, OUT_TEMP_L_XM, temp, 2);
    int16_t temperature = (temp[1] << 8) | temp[0];

    return temperature;
}

array<int16_t, AxisDOF> LsmController::readSensor(uint8_t i2cAddress, uint8_t regAddress) const
{
    uint8_t temp[6];
    readRegs(i2cAddress, regAddress, temp, 6);
    int16_t x_raw = (int16_t)(((int16_t)temp[1] << 8) | temp[0]);
    int16_t y_raw = (int16_t)(((int16_t)temp[3] << 8) | temp[2]);
    int16_t z_raw = (int16_t)(((int16_t)temp[5] << 8) | temp[4]);
    array<int16_t, AxisDOF> raw = { x_raw, y_raw, z_raw };

    return raw;
}

array<int16_t, AxisDOF> LsmController::readMag() const
{
    array<int16_t, AxisDOF> rawMag = readSensor(m_xmAddress, OUT_X_L_M);
    
    if (m_calibrateMag) {
        for (auto i = 0; i < AxisDOF; ++i) {
            rawMag[i] -= m_magCalibration.bias[i];
        }

        array<int16_t, AxisDOF> calibrated;
        for (auto i = 0; i < AxisDOF; ++i) {
            calibrated[i] = 0;
            for (auto j = 0; j < AxisDOF; ++j) {
                calibrated[i] += m_magCalibration.transform[i][j] * rawMag[j];
            }
        }

        rawMag = calibrated;
    }

    return rawMag;
}

array<int16_t, AxisDOF> LsmController::readGyro() const
{
    return readSensor(m_gAddress, OUT_X_L_G);
}

array<int16_t, AxisDOF> LsmController::readAccel() const
{
    return readSensor(m_xmAddress, OUT_X_L_A);
}