#include "server.h"

#include <chrono>
#include <sstream>
#include <iostream>
#include <unistd.h>

using namespace std;
using namespace zmq;

Server::Server() :
    m_uartThread(&Server::handleUart, this, m_uartPort),
    m_compassThread(&Server::handleCompass, this, m_lsmBus, m_compassTimeout),
    m_context(unique_ptr<context_t>(new context_t(1))),
    m_clientReply(unique_ptr<socket_t>(new socket_t(*m_context, ZMQ_REP))),
    m_clientRequest(unique_ptr<socket_t>(new socket_t(*m_context, ZMQ_REQ))),
    m_clientPublish(unique_ptr<socket_t>(new socket_t(*m_context, ZMQ_PUB))),
    m_statusPublish(unique_ptr<socket_t>(new socket_t(*m_context, ZMQ_PUB))),
    m_statusTimer(shared_ptr<Timer>(new Timer(m_statusTimeout))),
    m_publishTimer(shared_ptr<Timer>(new Timer(m_publishTimeout)))
{
    // Set high water mark (max number of outstanding msgs in memory)
    try {
        int hwmOption = 5;
        m_statusPublish->setsockopt(ZMQ_SNDHWM, &hwmOption, sizeof(int));
    }
    catch (const exception &e) {
        LOG << "SETSOCKOPT FAILED" << endl;
    }

    // Initialize zmq communication endpoints
    m_clientReply->bind(m_clientReplyPipe.c_str());
    m_clientPublish->bind(m_clientPublishPipe.c_str());
    m_statusPublish->bind(m_statusPublishPipe.c_str());
    m_clientRequest->connect(m_clientRequestPipe.c_str());

    // Initialize depth converter
    m_lcDepth.setValues(m_depthX1, m_depthX2, m_depthY1, m_depthY2);
}

Server::~Server()
{
    m_isRunning = false;
    m_uartThread.join();
    m_compassThread.join();
}

Server& Server::instance()
{
    static Server m_instance;
    return m_instance;
}

void Server::start()
{
    m_statusTimer->start();
    m_publishTimer->start();
}

void Server::update()
{
    message_t clientMsg;
    // Check for income zmq messages, nonblock way 
    if (m_clientReply->recv(&clientMsg, ZMQ_NOBLOCK)) {
        if (clientMsg.size() == THRUSTERS_MSG_SIZE) {
            updateThrustersPower(clientMsg);
        }
        else if (clientMsg.size() == EXEC_SERVER_MSG_SIZE) {
            handleExecServerCmd(clientMsg);
        }
    }

    if (m_statusTimer->isCompleted()) {
        updateAndPublishStatus();
    }

    if (m_publishTimer->isCompleted()) {
        updateAdcData();
        m_uartRequestStorage.altimeterInfoRequest();
        publishVehicleInfo();

        // Check if button was pressed
        if (m_adcInfo.buttonStatus != 0) {
            publishButtonPressed();
            m_adcInfo.buttonStatus = 0; // reset button state
        }
    }
}

void Server::updateAdcData()
{
    m_uartRequestStorage.supervisorInfoRequest();
    m_adcInfo = m_uartReplyStorage.getAdcInfo();
}

void Server::updateDevicesStatus()
{
    m_uartRequestStorage.devicesStatusRequest();
}

void Server::handleExecServerCmd(message_t &message)
{
    uint8_t cmd;
    memcpy(&cmd, message.data(), sizeof(uint8_t));

    m_uartRequestStorage.missionStatusRequest(cmd);

    message_t reply (sizeof(uint8_t));
    memset(reply.data(), 0xFF, sizeof(uint8_t));
    m_clientReply->send(reply);
}

void Server::updateThrustersPower(message_t &message)
{
    ThrustersPower power;
    memcpy(&power, message.data(), message.size());
    
    m_uartRequestStorage.thrustersPowerRequest(power);

    message_t reply (sizeof(uint8_t));
    memset(reply.data(), 1, sizeof(uint8_t));
    m_clientReply->send(reply);
}

void Server::publishVehicleInfo()
{
    VehicleInfo info;
    m_compassDataMutex.lock();
    info.yaw = m_compassInfo.yaw;
    info.roll = m_compassInfo.roll;
    info.pitch = m_compassInfo.pitch;
    m_compassDataMutex.unlock();
    info.depth = m_lcDepth.convert(m_adcInfo.depth);

    message_t vehicleInfoMsg(sizeof(VehicleInfo));
    memcpy((void *)vehicleInfoMsg.data(), (void *)&info, sizeof(VehicleInfo));
    m_clientPublish->send(vehicleInfoMsg);
}

void Server::publishButtonPressed()
{
    uint8_t buttonCode = 15;
    message_t msg (sizeof(uint8_t));
    memcpy((void *)msg.data(), &buttonCode, sizeof(uint8_t)); // send 1 byte
    m_clientRequest->send(msg);
    
    message_t reply;
    m_clientRequest->recv(&reply);
}

void Server::updateAndPublishStatus()
{
    updateDevicesStatus();
    auto devicesStatus = m_uartReplyStorage.getDevicesStatus();

    StatusInfo status;
    memcpy((void *)status.devicesTypes, (void *)devicesStatus.data(), sizeof(devicesStatus));
    m_compassDataMutex.lock();
    status.yaw = m_compassInfo.yaw;
    status.roll = m_compassInfo.roll;
    status.pitch = m_compassInfo.pitch;
    m_compassDataMutex.unlock();
    status.depth = m_lcDepth.convert(m_adcInfo.depth);
    status.leak = m_adcInfo.leakStatus;
    status.version = m_version;
    status.cameras = getCamerasStatus();

    message_t statusMsg(sizeof(status));
    memcpy((void *)statusMsg.data(), (void *)&status, sizeof(status));
    m_statusPublish->send(statusMsg);
}

uint8_t Server::getCamerasStatus()
{
    uint8_t cameras_counter = 0;
    // Check availability of /dev/videoX file, X = [0, 1]
    for (int i = 0; i <= 1; ++i) {
        stringstream ss;
        ss << "/dev/video" << i;
        if (access(ss.str().c_str(), F_OK) != -1) {
            cameras_counter++;
        }
    }

    return cameras_counter;
}

void Server::handleUart(string port)
{
    UartManager uart(port, m_uartReplyStorage, m_uartRequestStorage);

    while (m_isRunning) {
        // Send uart msg if exists and uart port is ready
        if (!uart.isNoRequests() && uart.isReady()) {
            uart.execRequest();
        }

        this_thread::sleep_for(chrono::milliseconds(1));
    }
}

void Server::handleCompass(int bus, unsigned int readTimeout)
{
    Timer compassTimer(readTimeout);
    compassTimer.start();

    LsmController lsmController(bus);
    LsmConverter lsmConverter(lsmController.getSettings());

    while (m_isRunning) {
        if (compassTimer.isCompleted()) {
            int16_t temp = lsmController.readTemperature();
            array<int16_t, AxisDOF> mag = lsmController.readMag();
            array<int16_t, AxisDOF> gyro = lsmController.readGyro();
            array<int16_t, AxisDOF> accel = lsmController.readAccel();
            lsmConverter.setCompassData(accel, gyro, mag);
        }
        
        EulerAngles angles = lsmConverter.convertToAngles();

        m_compassDataMutex.lock();
        m_compassInfo.yaw = angles.yaw;
        m_compassInfo.roll = angles.roll;
        m_compassInfo.pitch = angles.pitch;
        m_compassInfo.dus = lsmConverter.getCalcData().gz;
        m_compassDataMutex.unlock();

        this_thread::sleep_for(chrono::milliseconds(1));
    }
}