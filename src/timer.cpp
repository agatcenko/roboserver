#include "timer.h"

using namespace std;

Timer::Timer(unsigned int timeout) :
    m_timeout(timeout)
{

}

Timer::~Timer()
{

}

void Timer::reset()
{
    m_start = chrono::steady_clock::now();
}

void Timer::start()
{
    reset();
}

bool Timer::isCompleted()
{
    auto diff = chrono::steady_clock::now() - m_start;
    auto msec = chrono::duration_cast<chrono::milliseconds>(diff);
    if (msec >= chrono::milliseconds(m_timeout)) {
        reset();
        return true;
    }

    return false;
}

bool OneShotTimer::isCompleted()
{
    auto diff = chrono::steady_clock::now() - m_start;
    auto msec = chrono::duration_cast<chrono::milliseconds>(diff);
    if (msec >= chrono::milliseconds(m_timeout)) {
        return true;
    }

    return false;
}