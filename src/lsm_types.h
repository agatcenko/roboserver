#pragma once

// Number of degrees of freedom
const int AxisDOF = 3;

enum GyroScale
{
    G_SCALE_245DPS,
    G_SCALE_500DPS,
    G_SCALE_2000DPS
};
  
enum AccelScale
{
    A_SCALE_2G,
    A_SCALE_4G,
    A_SCALE_6G,
    A_SCALE_8G,
    A_SCALE_16G
};

enum MagScale
{
    M_SCALE_2GS,
    M_SCALE_4GS,
    M_SCALE_8GS,
    M_SCALE_12GS
};

enum GyroODR
{
    // Hz - Bandwidth
    G_ODR_95_BW_125  = 0x0,
    G_ODR_95_BW_25   = 0x1,
    G_ODR_190_BW_125 = 0x4,
    G_ODR_190_BW_25  = 0x5,
    G_ODR_190_BW_50  = 0x6,
    G_ODR_190_BW_70  = 0x7,
    G_ODR_380_BW_20  = 0x8,
    G_ODR_380_BW_25  = 0x9,
    G_ODR_380_BW_50  = 0xA,
    G_ODR_380_BW_100 = 0xB,
    G_ODR_760_BW_30  = 0xC,
    G_ODR_760_BW_35  = 0xD,
    G_ODR_760_BW_50  = 0xE,
    G_ODR_760_BW_100 = 0xF
};

enum AccelODR
{
    // Hz
    A_POWER_DOWN,
    A_ODR_3125, // 3.125 Hz
    A_ODR_625,  // 6.25  Hz
    A_ODR_125,  // 12.5  Hz
    A_ODR_25,   // 25    Hz
    A_ODR_50,
    A_ODR_100,
    A_ODR_200,
    A_ODR_400,
    A_ODR_800,
    A_ODR_1600
};

enum MagODR
{
    M_ODR_3125,   // 3.125 Hz (0x00)
    M_ODR_625,    // 6.25 Hz (0x01)
    M_ODR_125,    // 12.5 Hz (0x02)
    M_ODR_25, // 25 Hz (0x03)
    M_ODR_50, // 50 (0x04)
    M_ODR_100 // 100 Hz (0x05)
};

enum AccelABW
{
    A_ABW_773, // 773 Hz (0x0)
    A_ABW_194,
    A_ABW_362,
    A_ABW_50
};

struct LsmSettings
{
    GyroScale gScale;
    AccelScale aScale;
    MagScale mScale;
    GyroODR gODR; 
    AccelODR aODR; 
    MagODR mODR;
    AccelABW aABW;
    int gyro_bias[3];
    int accel_bias[3];
};
