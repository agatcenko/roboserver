#pragma once

#include "msg_types.h"

#include <set>
#include <mutex>
#include <queue>

//! Enum with types of output uart messages 
enum class RequestType : uint8_t
{
    DEVICES_STATUS,  /*!< Request devices status */
    ALTIMETER_INFO,  /*!< Request altimeter data */
    SUPERVISOR_INFO, /*!< Request supervisor data */
    DEVICE_TYPE,     /*!< Request device type */
    MISSION_STATUS,  /*!< Send mission status */
    THRUSTERS_POWER, /*!< Send thrusters power */
    UNKNOWN_REQUEST  /*!< Default request type */
};

/*!
    \brief Class that handle output uart messages
    
    Store output uart messages in queue
*/
class UartRequestStorage {
public:
    UartRequestStorage();

    void devicesStatusRequest(); //!< Store devices status request in queue if not present
    void altimeterInfoRequest(); //!< Store altimeter data request in queue if not present
    void supervisorInfoRequest(); //!< Store supervisor data request in queue if not present
    //! Store device type request in queue if not present
    /*! \param [in] id number identified device in system */
    void deviceTypeRequest(uint8_t id);
    //! Store mission status request in queue if not present
    /*! \param [in] status mission status */
    void missionStatusRequest(uint8_t status);
    //! Store thrusters power request in queue if not present
    /*! \param [in] power ThrusterPower struct with new values*/
    void thrustersPowerRequest(ThrustersPower power);

    bool isNoRequests() const; //!< Check if m_requests queue is empty

    RequestType getRequest(); //!< Get next request type from queue

    uint8_t getDeviceId() const; //!< Getter for device id
    uint8_t getMissionStatus() const; //!< Getter for mission status
    ThrustersPower getThrustersPower() const; //!< Getter for thrusters power

private:
    mutable std::mutex m_storageMutex; //!< Mutex object for work with queue
    std::queue<RequestType> m_requests; //!< Queue for requests
    std::set<RequestType> m_waitingRequests; //!< Store requests types in queue 

    uint8_t m_deviceId; //!< Id for device type request
    uint8_t m_missionStatus; //!< Mission status
    ThrustersPower m_thrustersPower; //!< Thrusters power

    void updateQueue(RequestType type); //!< Insert new request in queue
};