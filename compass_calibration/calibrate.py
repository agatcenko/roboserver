import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def prepare_data(data):
    limits = np.array([
        [min(data[:,0]), max(data[:,0])],
        [min(data[:,1]), max(data[:,1])],
        [min(data[:,2]), max(data[:,2])]])

    regularized = []

    center = np.array([
        np.average(limits[0]),
        np.average(limits[1]),
        np.average(limits[2])])

    centered_data = data - center

    distances = np.sqrt(centered_data[:, 0] ** 2. + centered_data[:, 1] ** 2. + centered_data[:, 2] ** 2.)
    coords = np.array([
        distances,
        np.arccos(centered_data[:, 2] / distances),
        np.arctan2(centered_data[:, 1], centered_data[:, 0])]).T

    sectors_num = 10
    theta_sectors = np.linspace(0, np.pi, num=sectors_num)
    phi_sectors = np.linspace(-np.pi, np.pi, num=(sectors_num * 2))

    for i in xrange(sectors_num - 1):
        for j in xrange(2 * sectors_num - 1):
            points_in_sector = []
            for k, point in enumerate(coords):
                if ((point[1] >= theta_sectors[i] and point[1] < theta_sectors[i + 1]) and
                    (point[2] >= phi_sectors[j] and point[2] < phi_sectors[j + 1])):

                    points_in_sector.append(data[k])

                if len(points_in_sector) > 0:
                    regularized.append(np.mean(np.array(points_in_sector), axis=0))

    return np.array(regularized)

def ellipsoid_fit(data):
    x = data[:, 0]
    y = data[:, 1]
    z = data[:, 2]

    D = np.array([x*x,
             y*y,
             z*z,
             2 * x*y,
             2 * x*z,
             2 * y*z,
             2 * x,
             2 * y,
             2 * z])
    DT = D.conj().T
    v = np.linalg.solve( D.dot(DT), D.dot( np.ones( np.size(x) ) ) )
    A = np.array(  [[v[0], v[3], v[4], v[6]],
                    [v[3], v[1], v[5], v[7]],
                    [v[4], v[5], v[2], v[8]],
                    [v[6], v[7], v[8], -1]])

    center = np.linalg.solve(- A[:3,:3], [[v[6]],[v[7]],[v[8]]])
    T = np.eye(4)
    T[3,:3] = center.T
    R = T.dot(A).dot(T.conj().T)
    evals, evecs = np.linalg.eig(R[:3,:3] / -R[3,3])
    radii = np.sqrt(1. / evals)
    return center, radii, evecs, v

def plot_ellipsoid(center, radii, rotation, ax, plotAxes=False, cageColor='b', cageAlpha=0.2):
    phi = np.linspace(0., 2. * np.pi, 100)
    theta = np.linspace(0., np.pi, 100)

    x = radii[0] * np.outer(np.cos(phi), np.sin(theta))
    y = radii[1] * np.outer(np.sin(phi), np.sin(theta))
    z = radii[2] * np.outer(np.ones_like(phi), np.cos(theta))

    # rotate accordingly
    for i in xrange(len(x)):
        for j in xrange(len(x)):
            [x[i, j], y[i, j], z[i, j]] = np.dot([x[i, j], y[i, j], z[i, j]], rotation) + center

    if plotAxes:
        # make some purdy axes
        axes = np.array([[radii[0], 0., 0.],
                         [0., radii[1], 0.],
                         [0., 0., radii[2]]])
        # rotate accordingly
        for i in range(len(axes)):
            axes[i] = np.dot(axes[i], rotation)

        # plot axes
        for p in axes:
            x = np.linspace(-p[0], p[0], 100) + center[0]
            y = np.linspace(-p[1], p[1], 100) + center[1]
            z = np.linspace(-p[2], p[2], 100) + center[2]
            ax.plot(x, y, z, color=cageColor)

    # plot ellipsoid
    ax.plot_wireframe(x, y, z, rstride=4, cstride=4, color=cageColor, alpha=cageAlpha)


if __name__ == "__main__":
    data = np.loadtxt("raw_mag.txt")
    data = prepare_data(data)

    center, radii, evecs, v = ellipsoid_fit(data)

    centered_data = data - center.T

    a, b, c = radii
    r = (a * b * c) ** (1. / 3.)
    D = np.array([[r / a, 0., 0.], [0., r / b, 0.], [0. ,0. ,r / c]])
    TR = evecs.dot(D).dot(evecs.T)
    sphere_data = TR.dot(centered_data.T).T
    
    print
    print 'center: ',center
    print 'transformation:'
    print TR
    
    np.savetxt('transform.txt', np.vstack((center.T, TR)))

    fig = plt.figure(0)
    ax = fig.add_subplot(111, projection='3d')
    
    ax.set_aspect('equal')
    MAX = 200
    for direction in (-1, 1):
        for point in np.diag(direction * MAX * np.array([1,1,1])):
            ax.plot([point[0]], [point[1]], [point[2]], 'w')
    
    ax.scatter(centered_data[:, 0], centered_data[:, 1], centered_data[:, 2], marker='o', color='b')
    ax.scatter(sphere_data[:,0], sphere_data[:,1], sphere_data[:,2], marker='o', color='r')
    
    plot_ellipsoid([0, 0, 0], radii, evecs, ax=ax, plotAxes=True, cageColor='g')
    plot_ellipsoid([0, 0, 0], [r, r, r], evecs, ax=ax, plotAxes=True, cageColor="orange")

    plt.show()

