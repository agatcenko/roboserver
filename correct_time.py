import os

# Tool to change last modified time of files in current dir and subdirs.
# Need when intel edison reset its date, so files get future dates.

for (dir, _, files) in os.walk(os.path.dirname(os.path.abspath(__file__))):
    for file_name in files:
        path = os.path.join(dir, file_name)
        os.utime(path, None) # Set modified time to current system time